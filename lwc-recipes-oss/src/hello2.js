import { buildCustomElementConstructor, register } from 'lwc';
import { registerWireService } from '@lwc/wire-service';
import UiApp from 'recipe/hello';

registerWireService(register);

customElements.define('recipe-hello', buildCustomElementConstructor(UiApp));
